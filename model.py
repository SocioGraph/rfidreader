import pickle
import redis
import uuid


redis_db=None
class Model(object):
    def __init__(self, name):
        self.name = name.lower()
        self.id_attr = f"{name.lower()}_id" 

    def get(self, id):
        obj = redis_db.hget(self.name, id)
        if obj:
            return pickle.loads(obj)
        return None 

    def post(self, data):
        if self.id_attr not in data:
            uid = str(uuid.uuid4())
            data[self.id_attr] = uid
        else:
            uid = data[self.id_attr]
        redis_db.hset(self.name, uid, pickle.dumps(data))
        return data

    def update(self, id, data):
        o = self.get(id)

        if o:
            data[self.id_attr]= id
            o.update(data)
            return self.post(o)
        return o

    def delete(self, id):
        n = redis_db.hdel(self.name, *[id])
        if n:
            return id
        return None


    def list(self, **kwargs):
        obs = []
        for i in redis_db.hgetall(self.name):
            o = self.get(i)
            if o:
                match=True
                for k, v in kwargs.items():
                    if o.get(k) != v:
                        match = False
                        break
                if match:
                    obs.append(o)
        return {"data": obs}


if __name__ == "__main__":
    redis_db= redis.StrictRedis(host='localhost', port=6379, db=0)

    m = Model("Video")
    m.post({
        "video_name": "Video 1",
        "url": "https://ggogle.com"
    })
    obs = m.list(**{"video_name": "Video 2"})
    print(obs)
    # print(o)
    print(m.delete("03090dd4-afc6-4d26-9e62-3ecf8c2ef4a0"))
        