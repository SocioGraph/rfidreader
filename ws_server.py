from flask import Flask, render_template, request, url_for
from flask_socketio import SocketIO, disconnect as socketDisconnect
import multiprocessing
import base64, os, time, string, random, argparse, traceback, json
import logging, sys

# logging.getLogger('socketio.server').setLevel(logging.DEBUG)
# logging.getLogger('engineio.server').setLevel(logging.DEBUG)
global logg
global MEDIA_MAP
MEDIA_MAP = {}

LOG_DIR = os.environ.get("DAVE_LOG_DIR", "./logs")
if not os.path.isdir("./logs"):
    print(f"Creating {LOG_DIR} dir")
    os.mkdir("./logs")

file_handler = logging.FileHandler(filename='./logs/tmp.log')
stdout_handler = logging.StreamHandler(sys.stdout)

handlers = [file_handler, stdout_handler]

logging.basicConfig(
    level=logging.DEBUG, 
    format='[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s',
    handlers=handlers
)

logg = logging.getLogger('ws_streamer')
    
app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'

global bgprocess
bgprocess = None
bg_results_list = None

sio = SocketIO(app, logger=False, engineio_logger=False, async_mode="eventlet", cors_allowed_origins="*")

clients = None
sessions = {}

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def b64Decode(base64String):
    return base64.b64decode(base64String.split(",")[1])

def getTaskDetails(scanned_code, scanner_id):
    return MEDIA_MAP.get(scanned_code, {})

def queueMonitor():
    while True:
        if not bgprocess.empty():
            try:
                task = bgprocess.get()
                logg.debug(f"Processing task {task}")
                media_info = getTaskDetails(task.get("scanned_code", ""), task.get("scanner_id", ""))
                
                if not media_info:
                    err = "Scanned ID not found."
                    logg.error(err)
                    task["err"] = err
                    continue

                task.update(media_info)
                logg.debug(f"Processed task: {task}.")
                bg_results_list.append(task)
            except Exception as e:
                logg.error("Exception")
                logg.error(e)
                task["error"] = str(e)
                bg_results_list.append(task)
                traceback.print_exc()
        else:
            pass
            #logg.info("QUEUE_OUT: queue is empty.")
         
        time.sleep(1)


@app.route('/', methods = ['GET'])
def home():
    # return "Nothing"
    return render_template('ws_polling.html')

@app.route('/statusz', methods = ['GET'])
def statusz():
    sha = "none"

    if not os.path.isfile("./rfidreader.sha"):
        return {"error":"SHA file not found."}

    with open("./streamer.sha", "r") as fp:
        sha =  fp.read()

    return { "version": sha }

@sio.on('connect')
def connect():
    logg.info(request.args)
    if not "uid" in request.args:
        logg.info("Uid not sent, disconnecting.")
        socketDisconnect()

    clients[request.args.get("uid")]= request.sid
    print("client {} connected".format(request.sid))
    sio.emit("message","Ack.: Connection accepted", room=request.sid)

@sio.on('disconnect')
def disconnect():
    clientUid = request.args.get("uid")
    logg.info("popping from sessions")
    sessions.pop(clientUid, None) 
    logg.info(sessions)
    logg.info(f'Client {clientUid} disconnect')

@sio.on('astream')
def streamprocess(data):
    logg.info(f"Got data: {data}")
    if not "scanned_code" in data or not "scanner_id" in data:
        logg.info("Scanned code or scanner_id not present in data. Ignoring this scan.")
        return

    clientUid = request.args.get("uid")
    bgprocess.put(data)

@sio.on('results')
def serveIntermediateResults(data):
    logg.info("Searching for results")
    clientUid = request.args.get("uid")
    for elem in bg_results_list:
        logg.info(f"ELEM {elem}")
        logg.info(f"DATA {data}")
        if elem["screen_id"] == data["screen_id"]:
            print (f"RESULT OBJECT : {elem}")
            sio.emit("tasks", elem, room = clients[clientUid])
            bg_results_list.remove(elem)

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--PORT', help = "port to run", default = 5050)
    parser.add_argument('--MEDIA_MAP_FILE', help = "json file containing product and media file information.", default = "./mmf.json")
    args = parser.parse_args()

    mmf = args.MEDIA_MAP_FILE
    if not os.path.isfile(mmf):
        logg.error("Media file not found.")
        sys.exit()
    
    with open(mmf, "r") as fp:
        MEDIA_MAP = json.loads(fp.read())

    manager = multiprocessing.Manager()
    clients = manager.dict()
    bgprocess = manager.Queue()

    bg_results_list = manager.list()
    queueMonitor = multiprocessing.Process(name = "queueMonitor", target = queueMonitor)
    logg.info("Starting queue monitor.")
    queueMonitor.start()

    #sio.run(app, debug=True, port=args.PORT, host='0.0.0.0', certfile = './keys/server.crt', keyfile='./keys/server.key')
    sio.run(app, debug=True, port=args.PORT, host='0.0.0.0')

    # sio.run(app, debug=True, port=args.PORT, host='0.0.0.0',use_reloader=False)
