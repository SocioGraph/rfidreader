class Utilities {
    static extendData(data, extend_object){
        extend_object = extend_object || {};
        data = data || {};
        let myPromise = new Promise(function(myResolve, myReject) {
            if(typeof extend_object == 'function'){
                var extend_data = extend_object(data);
                if(typeof extend_data == "object"){
                    myResolve(extend_data);
                }else{
                    myReject("Failed to extend object");
                }
            }else if(typeof extend_object == 'string'){
                var exd = {};
                try {
                    exd = JSON.parse(extend_object);
                    var d = {...data, ...exd}
                    myResolve(d);
                } catch (e) {
                    myReject(e);
                }
            }else{
                var d = {...data, ...extend_object}
                myResolve(d);
            }
        });
        return myPromise;
    }

    static makeid(length) {
        let result           = '';
        const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
    
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
    
        return result;
    }

    static getTime() {
        return (new Date()).getTime()/1000;
    }

}

class WebsocketStreamer {
    
    recognition_sid = undefined;
    socketio = undefined;
    socket = undefined;
    uid = undefined;
    pollingObject = undefined;
        
    constructor(data) {
        console.info("Setting up streamer.");
        Object.assign(this, data);
        this.initSocket();
    }


    executeCallback(event, data) {
        try {
            this.event_callback(event,data)
        } catch (e) {
            console.log("Executing callback failed.");
            console.error(e);
        }
        
    }

    initEvents() {
        let that = this;
        this.streamDestination.on('connect', function(data) {
            that.executeCallback("wsconnect", "Websocket connection established.");
        });

     
        this.streamDestination.on('disconnect', function () {
            that.executeCallback("wsdisconnect", "Websocket connection is terminated.");
        });
        
        this.streamDestination.on('tasks', function (data) {
            console.log(data);
            if (!data.hasOwnProperty('type')) {
                console.error("Reveived task doesn't have typ attribute. Ignoring..");
            }
            that.executeCallback('task',data);
            // that.stopPolling(that.pollingObject);

        });
        
        this.streamDestination.on('error', function(data){
            console.log("backend error");
            if (that.onError && typeof(that.onError) == 'function' ) {
                that.executeCallback("error", data);
            }
        });

    }

    initSocket() {
        var that = this;

        this.uid = Utilities.makeid(6);

        console.log("Setting up Socketio.");

        this.streamDestination = io(this.server, {query:"uid="+this.uid});
        this.initEvents();
    }
    
    createPolling(uid, screen_id, interval = 2000) {
        var that = this;
        console.log("Creating polling");

        const intervalObject = setInterval(function() {
            console.log("Polling for tasks, every 2 seconds.");
            that.streamDestination.emit("results",{"uid":uid,"screen_id":screen_id});
        }, interval);
    
        return intervalObject;
    }

    stopPolling(intervalObject) {
        console.log("Stopping polling..");
        clearInterval(intervalObject);
        this.recognition_sid = undefined;
    }

    startPollingForMedia(screen_id) {
        this.pollingObject = this.createPolling(this.uid, screen_id);
    } 

} 
