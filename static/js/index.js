document.addEventListener('alpine:init', () => {
    Alpine.data('test', () => ({
        init(){
            this.updatedata()
        },
        open: false,
        state:"index",
        name: "",
        video_id:"",
        video_name:"",
        video_description:"",
        video_category:"",
        video_varient:"",
        video_view:0,
        video_date:"",

        // this is for device
        device_id:"",
        device_name:"",
        device_store_name:"",
        device_location:"",
        device_view:0,



        // This is for tag
        tag_id:"",
        tag_name:"",
        tag_videos:[],
        

        //fetching the data from the database
        fetchobjects( model_name,object_id){
            var url = "/objects/"+model_name+(object_id? "/"+object_id:"")
            return fetch(url).then((data)=>data.json());
        },
        

        // updating the data when object is created, edited, deleted.
        //this is for loading the data into lists created for storing objects.
        updatedata()
        {
            
            this.fetchobjects("video").then((data)=>{
                this.videos = data["data"];
                           
             })
             this.fetchobjects("device").then((data)=>{
                this.devices = data["data"];
                
             })
             this.fetchobjects("tag").then((data)=>{
                this.tags = data["data"];
             })
        },

        //posting objects
        sendingobjects(model_name,data_type)
        {
            var url = "/object/"+model_name;
           return fetch(url,{
                method: 'POST',
                headers: {'Content-Type':'application/json;charset=utf-8'
                },
                body: JSON.stringify(data_type)
                }).then((data) => data.json())
        },

        //patching objects
       async patchobjects(model_name, object_id,someData)
        {
            var url = "/object/"+model_name+ "/"+object_id
            return await fetch(url,{
                method: 'PATCH', 
                headers: {
                 'Content-type': 'application/json;charset=UTF-8' 
                },
                body: JSON.stringify(someData)
            }).then(respon => respon.json())
        },


        //deleting objects
        async  deleteobjects(model_name,object_id)
        {
            var url = "/object/"+model_name+ "/"+object_id
            return await fetch(url,{
                method: 'DELETE',
                headers: {
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({}),
            }).then((response)=>response.json())
        },

        //for popup for delete button 

        deletebtn(del_id, del_type){

            swal({
                title: "Are you sure you want to",
                text: "DELETE  ?",
                buttons:{
                    cancel: {
                      text: "Cancel",
                      value: false,
                      visible: true,
                      closeModal: true,
                      className : 'Cancelbtn',

                    },
                    confirm: {
                      text: "Delete",
                      value: true,
                      visible: true,
                      closeModal: true,
                      className : 'deletebtn',
                    },
                },
                dangerMode: true,

              })
              .then((confirm) => {
                if(confirm)
                {
                    console.log(del_id)
                    this.deleteobjects(del_type,del_id)
                    .then(()=>this.updatedata())
                        //location.reload();
                    
                }

              });
        },

        // this is for showing the videos
        Edit_add(vid){
            if(vid)
            {
                for(var i of this.videos){
                    if( i.video_id == vid){
                            for(var vd in i){
                                     this.video[vd] = i[vd]
                            }
                           break;
                    }
                }
                this.state = "Edit_Video";
                
            }
            else{
                for(var i in this.video){
                    this.video[i] = "";
               }
                this.state = "add_video";
            }
        },

        // this is for adding video objects 
        addingvideo(){
            var obj = {}
            // obj.video_id= 'vid'+this.videos.length;
            for(var i in this.video){ 
                obj[i] = this.video[i];
                obj.video_id= 'vid'+(this.videos.length+1);
                obj.video_view = 0;
                obj.video_date = new Date().toLocaleString('en-GB', { timeZone: 'IST' });
            
            }
                if (this.video.video_name!== "") {
                    // this.videos.push(obj);
                    this.sendingobjects("video", obj)
                    .then(()=>this.updatedata())
                    this.state = "index";
                }
                else{
                    alert("name is required field");
                }
                //location.reload();
        },

        // This is for editing video part and patching video edited object
        Editingvideo(){
            console.log(this.video.video_id);
            count = this.video.video_view+1;
            var date = new Date().toLocaleString('en-GB', { timeZone: 'IST' });

            var vid_data = {
                "video_id" : this.video.video_id,
                "video_name" : this.video.video_name,
               "video_category":  this.video.video_category,
               "video_varient" :this.video.video_varient,
               "video_description":this.video.video_description,
               "video_view":count,
               "video_date":date
               
         }
         this.patchobjects("video",this.video.video_id,vid_data).then(()=>this.updatedata());
         this.state = "index";
            // for(let k =0; k< this.videos.length; k++)
            // {
            //     if(this.videos[k].video_id == this.video.video_id)
            //     {
            //         this.videos[k].video_id = this.video.video_id;
            //         this.videos[k].video_name = this.video.video_name;
            //         this.videos[k].video_category = this.video.video_category;
            //         this.videos[k].video_varient = this.video.video_varient;
            //         break;
            //     }
            // }
            // //location.reload();
        },

        video:{
            video_id: "",
            video_src: "",
            video_name:"",
            video_description:"",
            video_category:"",
            video_varient:"",
            video_view:0,
            video_date:""

        },
        videos:[],
        

        // THIS IS FOR DEVICES

        Edit_add_device(divid){

            if(divid)
            {
                for(var i of this.devices){
                    if( i.device_id == divid){
                            for(var vd in i){
                                     this.device[vd] = i[vd];
                            }
                           break;
                    }
                }
                this.state = "Edit_Device";
            }
            else{
                for(var i in this.device){
                    this.device[i] = "";
               }
                this.state = "add_device";

            }

        },

        //This is for adding devices and posting device objects
        addingdevice(){
            var obj = {}
            for(var i in this.device){ 
                obj[i] = this.device[i];
                obj.device_id= 'device'+this.devices.length+1;
                obj.device_view=0;
            }
                if (this.device.device_name!== "") {
                    // this.devices.push(obj);
                    this.sendingobjects("device", obj).then(()=>this.updatedata())
                    this.state = "index";
                }
                else{
                    alert("device name is required field");
                }
                //location.reload();
        },
        Editingdevice(){
            this.state = "index";
            
            count = this.device.device_view+1;            
                var div_data={ 
                    "device_id": this.device.device_id,
                    "device_name": this.device.device_name,
                    "device_location": this.device.device_location,
                    "device_store_name": this.device.device_store_name,
                    "device_view":count
                }
                this.patchobjects("device",this.device.device_id,div_data).then(()=>this.updatedata())
                //location.reload();

        },
        device:{
            device_id:"",
            device_name:"",
            device_store_name:"",
            device_location:"",
            device_view:0
             },
             devices:[],
    

        // TAG PART STARTING 

        Edit_add_tag(tagid){

            if(tagid)
            {
                for(var i of this.tags){
                    if( i.tag_id == tagid){
                            for(var vd in i){
                                     this.tag[vd] = i[vd];
                            }
                           break;
                    }
                }
                this.state = "Edit_Tag";
            }
            else{
                for(var i in this.tag){
                    this.tag[i] = "";
                    this.tag.tag_id = "tag"+(this.tags.length+1);
               }
                this.state = "add_tag";

            }

        },

        //This is for adding Tags and posting to tag objects
        addingtag(){
            var obj = {}
            for(var i in this.tag){
                obj[i] = this.tag[i];
                obj.tag_id= 'tag'+(this.tags.length+1);
                // obj.tag_videos=[];
                // obj.tag_videos.push()
            }
                if (this.tag.tag_name!== "") {
                    // this.tags.push(obj);
                    this.sendingobjects("tag", obj).then(()=>this.updatedata())
                    this.state = "index";
                    this.vids = [];
                }
                else{
                    alert("tag name is required field");
                }
                //location.reload();
        },

        // This is for editing tags
        Editingtag(tags_id){
            var tag_data={ 
                "tag_id": this.tag.tag_id,
                "tag_name": this.tag.tag_name,
            }
            console.log(tags_id)
            this.patchobjects("tag",tags_id,tag_data).then(()=>this.updatedata())
            .then(()=>this.updatedata)
            this.state = "index";

            //location.reload();
        },
        vids: [],
        selectvideo(vid)
        {
          this.vids.push(vid);
          
          this.tag.tag_videos = this.vids
          console.log(this.tag.tag_videos);
        },
    // TAG PART

    tag:{
        tag_id:"",
        tag_name:"",
        tag_videos:[]
    },
    tags:[],
    }))
})


