from flask import session, request,Flask, redirect, url_for, render_template,jsonify,json
import os, uuid
import redis
import model
from model import Model


# Set the secret key to some random bytes. Keep this really secret!
app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/2343253kjhsoiuh92uhfdigufg'

#environ variables
SERVER_NAME = os.environ.get('SERVER_NAME', '__NULL__')
REDIS_SERVER_NAME = os.environ.get('REDIS_SERVER_NAME', 'localhost')
HTTP = os.environ.get('HTTP', 'http')

redis_db = redis.StrictRedis(host=REDIS_SERVER_NAME, port=6379, db=0)        
model.redis_db = redis_db


@app.after_request
def after_req(response):
    redis_db.save()
    # response.headers.add('Access-Control-Allow-Origin', '*')
    # print(response.status_code)
    return response


@app.route('/')
def index():
    if 'username' in session:
        return f'Logged in as {session["username"]}'
    return render_template("index.html")

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        session['username'] = request.form['username']
        return redirect(url_for('index'))
    return render_template("login.html")

@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    session.pop('username', None)
    return redirect(url_for('index'))


@app.route('/objects/<model_name>')
def objects(model_name, object_id=None):
    args = request.args.to_dict()
    model = Model(model_name, )
    return jsonify(model.list(**args))

    """
    vids= "data/"+model_name+".json"

    # devs= open('Data/devicesdata.json')
    # tgs= open('Data/tagsdata.json')
    # "/data/"+model+".json"

    # os.path.isfile() - this function returns true or false
    if(os.path.isfile(vids)):
        with open(vids,"r") as fp:
            obj = json.load(fp)
            if object_id:
                ob = list(filter( lambda x: x[obj['id_attr']] == object_id, obj.get("data", [])))
                if(len(ob)>0):
                    return jsonify(ob[0])
                else:
                    return jsonify(error = "object not found")
            return jsonify(obj) 
    else:
        return jsonify( error = "model not found"),404
    #read data/<model_name>.json
    #If file is not there return {}
    #Jsonify the response
    #{
    #    "id_attr": "video_id",
    #    "data":[]
    #}
    pass
    """

""""

@app.route('/object/<model_name>/<object_id>', methods=["DELETE"])
def deleting(model_name, object_id=None):
    vids= "data/"+model_name+".json"
    def write_file(objs):
        with open(vids, "w") as fp:
            json.dump(objs, fp, indent=2)
            
     
    
    if(os.path.isfile(vids)):  
        vals = request.json
        objs = None
        with open(vids,"r") as fp:
            objs = json.load(fp)
        
        ob = list(filter( lambda x: x[objs['id_attr']] != object_id, objs.get("data", [])))
        objs["data"] = ob
        write_file(objs=objs)
        return jsonify(message="Object deleted successfully")
    else:
        return jsonify(error="Model not found"), 404

"""

@app.route('/object/<model_name>', methods=["POST"])
@app.route('/object/<model_name>/<object_id>', methods=["GET", "PATCH", "DELETE"])
def model_object(model_name, object_id=None):
    m = Model(model_name)
    args = request.json
    if request.method == "GET":
        o = m.get(object_id)
        print(o,object_id)
        if o:
            return jsonify(o)
        return jsonify(error = "object not found")

    elif request.method == "DELETE":
        o = m.delete(object_id)
        if o:
            return jsonify(message = "object deleted successfully")
        return jsonify(error = "object not found")

    elif request.method == "PATCH":
        o = m.update(object_id, args)
        return jsonify(o)
    
    return jsonify(m.post(args))

    """
    if request.method == "POST" or request.method == "PATCH":
        vids= "data/"+model_name+".json"
        def write_file(objs):
            with open(vids, "w") as fp:
                json.dump(objs, fp, indent=2)
        if(os.path.isfile(vids)):  
            vals = request.json
            if not len(vals.keys()):
                return jsonify(error = "object body empty")
            objs = None
            with open(vids,"r") as fp:
                objs = json.load(fp)
            
            if object_id:
                ob = list(filter( lambda x: x[objs['id_attr']] == object_id, objs.get("data", [])))
                if(len(ob)>0):
                    ob[0].update(vals)
                    write_file(objs=objs)
                    return jsonify(ob[0])
                else:
                    return jsonify(error = "object not found")
            else:
                uid = uuid.uuid4()
                vals[objs["id_attr"]] = uid
                objs["data"].append(vals)
                write_file(objs=objs)
                return jsonify(vals)

        else:
            return jsonify(error="Model not found"), 404
    elif request.method=="DELETE":    
            vids= "data/"+model_name+".json"
            def write_file(objs):
                with open(vids, "w") as fp:
                    json.dump(objs, fp, indent=2)
                    
            
            
            if(os.path.isfile(vids)):  
                vals = request.json
                objs = None
                with open(vids,"r") as fp:
                    objs = json.load(fp)
                
                ob = list(filter( lambda x: x[objs['id_attr']] != object_id, objs.get("data", [])))
                objs["data"] = ob
                write_file(objs=objs)
                return jsonify(message="Object deleted successfully")
            else:
                return jsonify(error="Model not found"), 404

    """


if __name__ == '__main__':
    app.run(port=5001, debug=True)