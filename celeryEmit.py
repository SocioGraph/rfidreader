
from celery import Celery
from flask_socketio import SocketIO, disconnect as socketDisconnect


celery = Celery('demo', broker='redis://')
sockio = SocketIO(message_queue = 'redis:///socketio')

@celery.task()
def send_tasks(event, message, client_id):
    sockio.emit("event", message, room=client_id)
    sockio.sleep()
