import socketio

csio = socketio.Client()

scanner_id = 9874561

@csio.on('connect')
def on_connect():
    print("Connected")
    csio.send(f"\nClient {csio.sid} connected...\n")

@csio.on('disconnect')
def on_disconnect():
    print("Disconnect")
    
@csio.on('result')
def receive_custom(msg):
    print(msg)

csio.connect(f'http://0.0.0.0:5050?uid={scanner_id}')
# sio.wait() # cannot keyboard interrupt this

dsecs = 1
print("Auto disconnect after 10s")
while dsecs:
    csio.emit('astream', {"scanned_code": 123588, "scanner_id" : scanner_id})
    dsecs = dsecs - 1
    csio.sleep(1)


csio.disconnect()
