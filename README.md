# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration

* Dependencies
    Python==3.8
    Python-pip
    virtualenv (optional)
    Refer requirements.txt for library dependencies.

* Database configuration
    Null

* How to run tests
    To be set up

* Deployment instructions
    1. `virtualenv pyenv --python=python3.8`
    2. `source pyenv/bin/activate`
    3. `pip install -r requirements.txt`
    4. `python ws_server.py`

* Deployment instructions - for example client code
    1. `cd example/`
    2. `virtualenv cpyenv --python=python3.8`
    3. `source cpyenv/bin/activate`
    4. `pip install -r requirements.txt`
    5. `python client.py`
    
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
    
* Other community or team contact