from flask import Flask, render_template, redirect, url_for, request
import sqlite3 as sql
# from flask_sqlalchemy import SQLALchemy
from werkzeug.utils import secure_filename

app = Flask(__name__, template_folder='templates')
# app.config['SQLALCHEMY_DATABASE_URI']= "sqlite:///db.sqlite3"
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# db = SQLAlchemy(app)
 #creating db
# conn = sql.connect('database.db')
# print("Opened database successfully");

# conn.execute('CREATE TABLE rfvds (name TEXT, rfid TEXT, file BLOB)')
# print("Table created successfully");
# conn.close()

# @app.route('/')
# def hello_world():
#    return "Hello world"

@app.route('/abc')
def my_name():
   return "My Name is Abhi"

# Route for handling the login page logic
@app.route('/', methods=['GET', 'POST'])
def login():
   error = None
   if request.method == 'POST':
      if request.form['username'] != 'admin' or request.form['password'] != 'admin':
         error = 'Invalid Credentials. Please try again.'
      else:
         return redirect(url_for('my_name'))
   return render_template('login1.html')


#sql code starts here.
# @app.route('/')
# def home():
#    return render_template('login1.html')

@app.route('/enternew')
def new_student():
   return render_template('student.html')

@app.route('/addrec',methods = ['POST', 'GET'])
def addrec():
   if request.method == 'POST':
      try:
         nm = request.form['nm']
         rfid = request.form['rfid']
         # city = request.form['city']
         # pin = request.form['pin']

         f = request.files['file']
         # f.save(secure_filename(f.filename))
         # return 'file uploaded successfully'
         
         with sql.connect("database.db") as con:
            cur = con.cursor()
            
            cur.execute("INSERT INTO rfvds (name,rfid,file) VALUES (?,?,?)",(nm,rfid,f) )
            
            con.commit()
            msg = "Record successfully added"
      except:
         con.rollback()
         msg = "error in insert operation"
      
      finally:
         return render_template("result.html",msg = msg)
         con.close()

@app.route('/list')
def list():
   con = sql.connect("database.db")
   con.row_factory = sql.Row
   
   cur = con.cursor()
   cur.execute("select * from students")
   
   rows = cur.fetchall();
   return render_template("list.html",rows = rows)


if __name__ == '__main__':
   app.config["UPLOAD_FOLDER"] = "/home/abhijeet/rfidreader/my_work/templates"
   app.run(debug = True)

##requirent.txt for client.py
# bidict==0.21.4
# certifi==2021.10.8
# charset-normalizer==2.0.12
# idna==3.3
# netifaces==0.10.6
# python-engineio==3.14.2
# python-socketio==4.6.1
# requests==2.27.1
# six==1.16.0
# urllib3==1.26.8
# websocket-client==1.3.1
